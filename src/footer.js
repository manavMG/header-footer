import React from "react";

function Footer(props) {    
    if(props && props.hidden){
        return null;
    }
    return (
        <div style={{
            background: "brown",
            width: "100%",
            overflow: "hidden",
            position: "sticky",
            bottom: "0"
        }}>
            <h1>This is footer</h1>
        </div>
    );
}

export default Footer;