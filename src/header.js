import React from "react";

function Header(props) {
    if(props && props.hidden){
        return null;
    }
    return (
        <div style={{
            background: "brown",
            width: "100%",
            overflow: "hidden",
            position: "sticky",
            top: "0"
        }}>
            <h1>This is header</h1>
        </div>
    );
}
export default Header;