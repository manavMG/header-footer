import React, { Component } from "react";
import "./index.css";
import x from './text'
import { withStyles } from "@material-ui/core/styles"

const styles = theme => ({
  
  header: {
    background: "brown",
    width: "100%",
    overflow: "hidden",
    position: "sticky",
    top: "0",
    willChange: "transform",
    transition: "transform 200ms linear"
  },
  footer: {
    background: "brown",
    width: "100%",
    overflow: "hidden",
    position: "sticky",
    bottom: "0",
    willChange: "transform",
    transition: "transform 200ms linear"
  }
})

class App extends Component {
  constructor(props) {
    super(props);
    this.prevScrollpos = window.pageYOffset
    this.state = {
      hidden: false,
    }
  }
  componentDidMount() {
    document.addEventListener('scroll', this.handleScroll, true)
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }
  handleScroll(e) {
    console.log("Scrolling")
    var currentScrollPos = window.pageYOffset;
    if (this.prevScrollpos > currentScrollPos) {
      document.getElementById("header").style.transform = "none";
      document.getElementById("footer").style.transform = "none";
    } else {
      document.getElementById("header").style.transform = "translateY(-100%)";
      document.getElementById('footer').style.transform = "translateY(100%)"
    }
    this.prevScrollpos = currentScrollPos
  }

  render() {
    const { classes } = this.props
    return (
      <div>
        {/* <button onClick={() => { 
         }}>Click me</button> */}
        <div id="header" className={classes.header} style={{
        }}>
          <h1>This is header</h1>
        </div>
        <div>
          {x}
        </div>
        <div id="footer" className={classes.footer} style={{

        }}>
          <h1>This is footer</h1>
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(App);